import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from sys import displayhook
from sklearn.pipeline import make_pipeline
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import confusion_matrix, classification_report
from sklearn.model_selection import train_test_split, learning_curve
from sklearn.ensemble import IsolationForest
from sklearn.neighbors import KNeighborsClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier
import pickle
import numpy as np

### Remove outliers and split dataframe into test and train sets
def pre_processing (df, target) :

    pd.options.mode.chained_assignment = None

    train_df, test_df = train_test_split(df, test_size=0.2, random_state=0)

    isolation_model = IsolationForest(contamination=0.05)

    isolation_model.fit(train_df)
    outliers = isolation_model.predict(train_df)
    train_df["outlier"] = outliers
    train_df = train_df[train_df['outlier'] == 1]
    train_df.drop(["outlier"], axis=1, inplace=True)
    train_df.reset_index(drop=True, inplace=True)

    outliers = isolation_model.predict(test_df)
    test_df["outlier"] = outliers
    test_df = test_df[test_df['outlier'] == 1]
    test_df.drop(["outlier"], axis=1, inplace=True)
    test_df.reset_index(drop=True, inplace=True)

    X_train = train_df.drop([target], axis = 1)
    y_train = train_df[target]

    X_test = test_df.drop([target], axis = 1)
    y_test = test_df[target]

    return X_train, y_train, X_test, y_test

def evaluation(model, df, target) :
    
    X_train, y_train, X_test, y_test = pre_processing (df, target)

    model.fit(X_train, y_train)
    y_pred = model.predict(X_test)

    print(f"Train Score = {round(model.score(X_train, y_train) * 100, 2)}")

    plt.figure(figsize=(8,8))
    plt.style.use('dark_background')
    plt.title("Confusion matrix :\n")
    sns.heatmap(confusion_matrix(y_test, y_pred),cmap = "plasma", annot=True, fmt="d")
    plt.xlabel("Reality")
    plt.ylabel("Predictions")
    plt.show()

    print("\t> Classification test report\n")
    print(classification_report(y_test, y_pred))


def evaluation_with_learning_curve(model, df, target) :
    
    X_train, y_train, X_test, y_test = pre_processing (df, target)

    model.fit(X_train, y_train)
    y_pred = model.predict(X_test)

    print(f"\n\t==> Train Score = {round(model.score(X_train, y_train) * 100, 2)}\n\n")

    plt.figure(figsize=(8,8))
    plt.style.use('dark_background')
    plt.title("Confusion matrix :\n")
    sns.heatmap(confusion_matrix(y_test, y_pred),cmap = "plasma", annot=True, fmt="d")
    plt.xlabel("Reality")
    plt.ylabel("Predictions")
    plt.show()

    print("\t> Classification test report\n")
    print(classification_report(y_test, y_pred))

    N, train_score, validation_score = learning_curve(model, X_train, y_train, cv=4, scoring='f1', train_sizes=np.linspace(0.1,1,10))
    
    plt.figure(figsize=(10,10))
    plt.style.use('dark_background')
    plt.title("Learning curve :\n")
    plt.plot(N, train_score.mean(axis = 1), label = "train score", c="r")
    plt.plot(N, validation_score.mean(axis = 1), label = "valisation score", c="w")
    plt.legend()
    plt.xlabel("Learning (f1 score)")
    plt.ylabel("Time")
    plt.show()

def model_training(model, df, target) :
    
    X_train, y_train, X_test, y_test = pre_processing (df, target)

    model.fit(X_train, y_train)
    y_pred = model.predict(X_test)

    print("\t> Classification test report\n")
    print(classification_report(y_test, y_pred))

    return model
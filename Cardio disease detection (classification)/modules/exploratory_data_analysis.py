import random
import pandas as pd
from sys import displayhook
import seaborn as sns
import numpy as np
from matplotlib import pyplot as plt, cm
from scipy.stats import ttest_ind
from statistics import mode
import warnings

# hide warning because of sns.distplot warning spam
warnings.filterwarnings('ignore')


#####################################################################
#----------------- Global dataframe shape analysis -----------------#
#####################################################################

#data set global shape analysis :
def get_data_set_shape(df) :

    try :
        print("Data set shape :")
        print(f"\t> Rows count : {df.shape[0]}")
        print(f"\t> Columns count : {df.shape[1]}")

        print("\nData set types :")
        for col, count in zip(df.dtypes.value_counts().index, df.dtypes.value_counts()) :
            print(f"\t> {str(col)+' ':-<20}> {count}")
    
    except Exception as e :
        print("\t> Something is wrong !")
        return e

#############################################################################
#############################################################################

# NaN analysis :
def nan_analysis (df) :

    try :
        plt.figure(figsize=(7,7))
        plt.style.use('dark_background')
        plt.title("NaN repartition\n")
        sns.heatmap(df.isna(), cmap = 'Reds')
        plt.show()

        missing_values_rate = round((df.isna().sum().sum() / (df.shape[0] * df.shape[1]) * 100),2)
        print(f"Missing values rate into the whole data set : \n\t> {missing_values_rate} %")

        nan_rates = round(((df.isna().sum() / df.shape[0])*100) , 2)
        nan_rates = nan_rates[(nan_rates > 0) & (nan_rates < 90)]
        nan_rates = nan_rates.sort_values(ascending = False)
        print("\nMissing values rate by column (columns with less than 90% missing values):")
        for col, count in zip(nan_rates.index, nan_rates) :
            print(f"\t> {str(col)+' ':-<50}> {count} %")
        
        useless_columns = round(((df.isna().sum() / df.shape[0])*100) , 2)
        useless_columns = useless_columns[useless_columns >= 90]
        useless_columns = useless_columns.sort_values(ascending = False)
        print(f"\nUseless columns (columns with more than 90% missing values):")
        for col, count in zip(useless_columns.index, useless_columns) :
            print(f"\t> {str(col)+' ':-<50}> {count} %")

    except Exception as e :
        print("\t> Something is wrong !")
        return e
    
#####################################################################
#----------------- Target and features exploration -----------------#
#####################################################################

# categorical variable visualisation and analysis using countplot from seaborn
def column_values_countplot (df, col, hue = None)  :

    try :
        plt.figure(figsize=(10,6))
        plt.style.use('dark_background')
        plt.title(f"{col} unique values counts\n")
        sns.countplot(data = df, x = col, hue = hue, palette="hot")
        plt.show()

        value_count = df[col].value_counts(normalize = True)
        for class_, rate in zip(value_count.index, value_count) :
            print(f"\t> {str(class_)+' ':-<20}> { round((rate*100),2) } %")

        print(f'\t> Unique values count {"":=>25}> {len(df[col].unique())}')
        print(f'\t> Unique values {"":=>25}> {df[col].unique()}')
        print(f'\t> Sample size {"":=>25}> {len(df[col])}')

    except Exception as e :
        print("\t> Something is wrong !")
        return e

#############################################################################
#############################################################################

# categorical variable visualisation and analysis using pie chart from pyplot
def column_values_pie (df, col) :

    try :
        plt.figure(figsize = (5,5))
        df[col].value_counts().plot.pie(autopct='%1.1f%%', textprops = dict(color ="black"))
        plt.legend()
        plt.show()

        print(f'\t> Unique values count {"":=>25}> {len(df[col].unique())}')
        print(f'\t> Unique values {"":=>25}> {df[col].unique()}')
        print(f'\t> Sample size {"":=>25}> {len(df[col])}')

    except Exception as e :
        print("\t> Something is wrong !")
        return e
    
#############################################################################
#############################################################################

# Numerical variable distribution visualisation and analysis using distplot from seaborn
# !!! To avoid warning spam, warnings were disabled !!!
def numerical_column_distribution (df, col) :

    try :
        plt.figure(figsize=(8,6))
        plt.style.use('dark_background')
        plt.title(f"{col} values distribution\n")

        sns.distplot(df[col], color="mediumspringgreen")
        sns.distplot(df[col], color="mediumspringgreen")
        
        plt.axvline(x = df[col].mean(),  c = 'white', lw = 2, label = "Mean")
        plt.axvline(x = df[col].median(),  c = 'white', ls = '--', label = "Median")
        plt.axvline(x = mode(df[col]),  c = 'white', ls = ':', label = "Mode")

        plt.legend()
        plt.show()

        if (df[col].mean() > df[col].median() > mode(df[col])) :
            print("POSITIVE SKEW !")
        
        elif (df[col].mean() < df[col].median() < mode(df[col])) :
            print("NEGATIVE SKEW !")
        
        elif (df[col].mean() == df[col].median() == mode(df[col])) :
            print("SYMMETRICAL DISTRIBUTION !")

        tmp_df = pd.DataFrame(df[col].describe().apply(lambda x : int(round(x,0))))
        data = {
            'Count values' : tmp_df[col]["count"],
            'Min value' : tmp_df[col]["min"],
            'Max value' : tmp_df[col]["max"],
            'Mean' : tmp_df[col]["mean"],
            'Q1' : tmp_df[col]["25%"],
            'Median (Q2)' : tmp_df[col]["50%"],
            'Q3' : tmp_df[col]["75%"],
            'Standard deviation' : tmp_df[col]["std"]
        }
        tmp_df = pd.DataFrame(data = data, index = [0])
        displayhook(tmp_df)

    except Exception as e :
        print("\t> Something is wrong !")
        return e

######################################################################
#----------------- Target and features correlations -----------------#
######################################################################

# numrical X / categorical Y correlation analysis (distribution of X depending on Y)
def numerical_X_distribution_depending_on_categorical_Y (df, x, y) :
    
    try :
        cmap = cm.get_cmap('jet_r')
        colors_list = np.arange(0,1, 0.1)

        plt.figure(figsize=(10,6))
        plt.style.use('dark_background')
        plt.title(f"{y} / {x} correlation\n")

        for value in df[y].unique() :
            c = random.choice(colors_list)
            color = cmap(c)
            colors_list = np.delete(colors_list, np.where(colors_list == c))
            sns.distplot(df[df[y] == value][x], label=value, color = color) 
        plt.legend()
        plt.show()

    except Exception as e :
            print("\t> Something is wrong !")
            return e

#############################################################################
#############################################################################

# numerical X / numerical Y correlation analysis (linear regression)
def numerical_X_numerical_Y_linear_correlation (df, x, y, hue = None) :

    if (df[x].dtypes != "object") & (df[y].dtypes != "object") :
        try :
            plt.figure(figsize=(15,10))
            plt.style.use('dark_background')
            sns.lmplot(data = df, x = x, y = y, hue = hue)
            plt.show()
            
        except Exception as e :
            print("\t> Something is wrong !")
            return e

    else :
        print(f"x and/or y variable are not numericals :\n\t> x => {df[x].dtypes}\n\t> y => {df[y].dtypes}")

#############################################################################
#############################################################################

# categorical X / categorical Y correlation analysis (crosstab between X and Y)
def categorical_X_Y_crosstab (df, x, y) :

    try :
        plt.figure(figsize=(10,6))
        plt.style.use('dark_background')
        plt.title(f"{y} / {x} correlation\n")
        sns.heatmap(pd.crosstab(df[y], df[x]), cmap = "plasma",  annot=True, fmt="d")
        plt.show()
    
    except Exception as e :
        print("\t> Something is wrong !")
        return e

#########################################################################################################
#----------------- Target and features correlation hypotheses statistical verification -----------------#
#########################################################################################################

# check if the mean of two distribution are significativly different using student T-test
def numerical_X_categorical_Y_student_t_test (df, x, y) :

    try :
        y_classes = df[y].unique()
        df_1 = df[ df[y] == y_classes[0] ]
        df_2 = df[ df[y] == y_classes[1] ]

        if ( (df_1.shape[0] > df_2.shape[0] * 0.9) & (df_1.shape[0] < df_2.shape[0] * 1.1) ) :
            try :
                alpha = 0.01
                stat, p = ttest_ind(df_1[x], df_2[x])
                
                if p < alpha :
                    return 'H0 rejected !'
                else :
                    return 0

            except Exception as e :
                print("\t> Something is wrong !")
                return e
        else :
            print("Student T-Test is not possible because unbalanced classes !!!")

    except Exception as e :
        print("\t> Something is wrong !")
        return e

############################################################
#----------------- Execution verification -----------------#
############################################################

if __name__ == "__main__" :
    
    # global shape analysis
    get_data_set_shape()
    nan_analysis ()

    # columns exploration
    column_values_countplot ()
    column_values_pie ()
    numerical_column_distribution ()

    # correlations
    numerical_X_distribution_depending_on_categorical_Y ()
    numerical_X_numerical_Y_linear_correlation ()
    categorical_X_Y_crosstab ()

    # hypotheses verification
    numerical_X_categorical_Y_student_t_test ()
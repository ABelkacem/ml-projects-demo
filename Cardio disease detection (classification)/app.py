from datetime import date
import pickle
import pandas as pd
import os
from datetime import date

os.chdir(os.path.dirname(os.path.abspath(__file__)))

with open("knn_model.pkl", "rb") as f :
    model = pickle.load(f)

def getDifference(then, now = date.today()):
    duration = now - then
    duration_in_s = duration.total_seconds()
    day_ct = 24 * 60 * 60

    def days():
      return divmod(duration_in_s, day_ct)[0]

    return int(days())

def level(value, normal_value) :

    try :
        if normal_value <= normal_value :
            return 1
        
        elif (value > normal_value) & (value <= normal_value * 1.25) :
            return 2
        
        elif value > normal_value * 1.25 :
            return 3

        else :
            return 0
    
    except Exception as e :
        print(f"error !!!\n\t> {e}")

def date_input() :

    verif = False
    attempt = 0

    while (verif == False) & (attempt < 5) :
        try :
            date_entry = input('Enter a date in YYYY-MM-DD format :')
            year, month, day = map(int, date_entry.split('-'))
            date_ = date(year, month, day)
            nb_days = getDifference(date_)
            return nb_days
        
        except Exception as e :
            print(f"Error !!!\n\t>{e}")
            print("Please enter a valide value !\n")
        
        attempt += 1

        if attempt >= 5 :
            print("You have exhausted all attempts !")
            exit()


def numerical_input(type_, message) :
    verif = False
    attempt = 0

    while (verif == False) & (attempt < 5) :
        try :
            value = type_(input(message))

            if type(value) != type_ :
                print("Please enter a valide value !\n")
                verif == False
            
            elif type(value) == type_ :
                print(type(value))
                return value
        
        except Exception as e :
            print(f"Error !!!\n\t>{e}")
            print("Please enter a valide value !\n")
        
        attempt += 1

        if attempt >= 5 :
            print("You have exhausted all attempts !")
            exit()


def get_user_data () :

    try :

        age_in_days = date_input()

        height = numerical_input(int, "Please enter your height in cm : ")
        weight = numerical_input(float, "Please enter your weight in kg : ")
    
        ap_hi = level(value = numerical_input(int, "Please enter your systolic blood pressur in mmHg : "), normal_value = 120)
        ap_lo = level(value = numerical_input(int, "Please enter your diastolic blood pressur in mmHg : "), normal_value = 80)
        
        cholesterol = level(value = numerical_input(int, "Please enter your total cholesterol level in mg/dL : "), normal_value = 239)
        gluc = level(value = numerical_input(int, "Please enter your glucose level in mg/dL : "), normal_value = 110)

        IBM = round( ( weight / ( ( height / 100)**2 ) ), 1)

        tmp_df = pd.DataFrame(
                data=[[age_in_days, height, weight, ap_hi, ap_lo, cholesterol, gluc, IBM]],
                columns=["date_of_birth", "height", "weight", "ap_hi", "ap_lo", "cholesterol", "gluc", "IBM"]
            )
        
        return tmp_df
        
    except Exception as e :
        print(f"Error !!!\n\t>{e}")

def pred(X) :
    return model.predict(X)

X = get_user_data ()

print('\n\n\nDataframe input :\n',X)
print(f"\n\n\nPrediction result : \n\t> {pred(X)}\n\n\n")


The objective of this application is to draw mall customers clusters.

How should we use our clustering application ?
    > Detect the best profitable cluster to focus the commercial companies on them.

Customers will be classified depending their :
    - Age and Annual Incomes
    - Age and Spending Score
    - Annual Incomes and Spending Score

==> The exploratory data analysis is detailed into "EDA_notebook.ipynb" file.
==> The model creation and application it's realeased into "Modeling_notebook.ipynb" file
==> All the functions are implemented into packages stored into "modules" folder
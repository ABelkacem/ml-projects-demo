import pandas as pd
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans

# Inertia elbow methode visualisation 
def KMeans_model_intertia_evaluation_by_clusters_number(df) :
    inertia = []
    for i in range(1, 11):
        model = KMeans(n_clusters=i, random_state=0)
        model.fit(df)
        inertia.append(model.inertia_)
          
    plt.figure(figsize=(10,5))
    plt.style.use('dark_background')
    plt.title(f'Elbow methode {df.columns[0]}/{df.columns[1]}')
    plt.plot(range(1, 11), inertia, c = 'lightblue')
    plt.scatter(range(1, 11), inertia, c = 'yellow', alpha = 0.5)
    plt.xlabel('Number of clusters')
    plt.ylabel('inertia')
    plt.show()

# K-Mean model creation and application
def kmeans(df, nb_clusters) :
    model = KMeans(n_clusters=nb_clusters, random_state=0)
    classes_col = model.fit_predict(df)
    classes_col = pd.DataFrame(data = classes_col, columns = ["Class"])
    df = pd.concat([df, classes_col], axis = 1)

    return model, df

# clusters visualisation
def plot_clusters(df, x, y, hue, model) :
    plt.figure(figsize=(5,5))
    plt.title(f"classification depending on {x} and {y}\n")
    colors = ["purple", "violet", "red", "orange", "yellow"]
    for i, color in zip(sorted(df[hue].unique()), colors) :
        tmp_df = df[df[hue] == i]
        plt.scatter(
            data=tmp_df,
            x = x,
            y = y,
            c = color,
            alpha=0.5,
            label = f"Class {i}"
        )

    plt.scatter(
        model.cluster_centers_[:,list(df.columns).index(x)],
        model.cluster_centers_[:, list(df.columns).index(y)],
        s = 100,
        c = 'white',
        marker=r'$\alpha$',
        label = 'Centeroid'
    )

    plt.xlabel(x)
    plt.ylabel(y)
    plt.legend(bbox_to_anchor=(1.05, 1), loc='upper left', borderaxespad=0)
    plt.show()
import random
import torch
import neattext.functions as ntx
from neattext import TextCleaner


#####################################################################
#----------------- Global dataframe shape analysis -----------------#
#####################################################################

#data set global shape analysis :
def get_data_set_shape(df) :

    try :
        print("Data set shape :")
        print(f"\t> Rows count : {df.shape[0]}")
        print(f"\t> Columns count : {df.shape[1]}")

        print("\nData set types :")
        for col, count in zip(df.dtypes.value_counts().index, df.dtypes.value_counts()) :
            print(f"\t> {str(col)+' ':-<20}> {count}")
    
    except Exception as e :
        print("\t> Something is wrong !")
        return e

#############################################################################
#############################################################################

# NaN analysis :
def nan_analysis (df) :

    try :
        missing_values_rate = round((df.isna().sum().sum() / (df.shape[0] * df.shape[1]) * 100),2)
        print(f"Missing values rate into the whole data set : \n\t> {missing_values_rate} %")

    except Exception as e :
        print("\t> Something is wrong !")
        return e
    
#####################################################################
#----------------- Target and features exploration -----------------#
#####################################################################

# print n random values into a column
def print_random_values(df, col, nb_values) :
    random.seed(0)
    random_tweets = random.sample(list(df[col]), nb_values)
    for tweet in random_tweets :
        print(f"\t> {tweet}\n")


############################################################
#----------------- Execution verification -----------------#
############################################################

# cleaning text pipeline
def cleaning_pipeline (text) :

    txt = ntx.TextFrame(text=text)
    docx = TextCleaner()
    
    docx.text = text
    docx.remove_urls()
    docx.remove_userhandles()
    docx.remove_hashtags()
    docx.remove_numbers()
    docx.remove_emails()
    docx.remove_special_characters()
    clean_text = docx.text
    return clean_text

# model application function
def model_application(text, model, tokenizer):
    
    tokens = tokenizer.encode(text, return_tensors='pt')
    result = model(tokens)
    return int(torch.argmax(result.logits))+1

############################################################
#----------------- Execution verification -----------------#
############################################################

if __name__ == "__main__" :
    
    get_data_set_shape()
    nan_analysis ()
    print_random_values()
    cleaning_pipeline()
    model_application()